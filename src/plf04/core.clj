(ns plf04.core)


(defn string-E-1
  [x]
  (letfn [(f [x y]
            (if (empty? x)
              (if (and (> y 0) (< y 4))
                true false)
              (if (= \e (first x))
                (f (rest x) (inc y))
                (f (rest x) y))))]
    (f x 0)))


(string-E-1 "Hello")
(string-E-1 "Heelle")
(string-E-1 "Heelele")
(string-E-1 "Hll")
(string-E-1 "e")
(string-E-1 "")


(defn string-E-2
  [x]
  (letfn [(f [x y acc]
            (if (empty? x)
              (if (and (> acc 0) (< acc 4))
                true false)
              (if (identical? y (first x))
                (f (subs x 1) y (inc acc))
                (f (subs x 1) y acc))))]
    (f x \e 0)))

(string-E-2 "Hello")
(string-E-2 "Heelle")
(string-E-2 "Heelele")
(string-E-2 "Hll")
(string-E-2 "e")
(string-E-2 "")



(defn string-times-1
  [x y]
  (if (== y 0)
    ""
    (str x(string-times-1 x (dec y)))))

(string-times-1 "Hi" 2)
(string-times-1 "Hi" 3)
(string-times-1 "Hi" 1)
(string-times-1 "Hi" 0)
(string-times-1 "Hi" 5)
(string-times-1 "Oh Boy!" 2)
(string-times-1 "x" 4)
(string-times-1 ""  4)
(string-times-1 "code" 2)
(string-times-1 "code" 3)


(defn string-times-2
  [x y acc]
  (if (==  y 0)
    acc
    (string-times-2 x (dec y) (str x acc))))

(string-times-2 "Hi" 2 "")
(string-times-2 "Hi" 3 "")
(string-times-2 "Hi" 1 "")
(string-times-2 "Hi" 0 "")
(string-times-2 "Hi" 5 "")
(string-times-2 "Oh Boy!" 2 "")
(string-times-2 "x" 4 "")
(string-times-2 ""  4 "")
(string-times-2 "code" 2 "")
(string-times-2 "code" 3 "")


(defn front-times-1
  [lengt c]
  (letfn [(f [x y]
            (if (< (count x) 3)
              (if (zero? y)
                ""
                (str lengt (f x (dec y))))
              (if (zero? y)
                ""
                (str (subs lengt 0 3) (f x (dec y))))))]
    (f lengt c)))


(front-times-1 "Chocolate" 2)
(front-times-1 "Chocolate" 3)
(front-times-1 "Abc" 3)
(front-times-1 "Ab" 4)
(front-times-1 "A" 4)
(front-times-1 "" 4)
(front-times-1 "Abc" 0)

(defn front-times-2
  [lengt c acc]
  (letfn [(f [x y z]
            (if (< (count x) 3)
              (if (zero? y)
                z
                (f x (dec y) (str x z)))
              (if (zero? y)
                z
                (f x (dec y) (str (subs x 0 3) z)))))]
    (f lengt c acc)))


(front-times-2 "Chocolate" 2 "")
(front-times-2 "Chocolate" 3 "")
(front-times-2 "Abc" 3 "")
(front-times-2 "Ab" 4 "")
(front-times-2 "A" 4 "")
(front-times-2 "" 4 "")
(front-times-2 "Abc" 0 "")



(defn count-XX-1
  [x]
  (if (empty? x)
    x
    (let [countXX-1 (fn [p] (= p [\x \x]))]
      (count (filter countXX-1 (map vector x (rest x)))))))

(count-XX-1 "abcxx")
(count-XX-1 "xxx")
(count-XX-1 "xxxx")
(count-XX-1 "abc")
(count-XX-1 "Hello there")
(count-XX-1 "Hexxo thxxe")
(count-XX-1 "")
(count-XX-1 "Kittens")
(count-XX-1 "Kittensxxx")


(defn count-XX-2
  [s acc]
  (if (empty? s)
    acc
    (+ acc (let [count-x-x-1 (fn [p] (= p [\x \x]))]
             (count (filter count-x-x-1 (map vector s (rest s))))))))

(count-XX-2 "abcxx" 0)
(count-XX-2 "xxx" 0)
(count-XX-2 "xxxx" 0)
(count-XX-2 "abc" 0)
(count-XX-2 "Hello there" 0)
(count-XX-2 "Hexxo thxxe" 0)
(count-XX-2 "" 0)
(count-XX-2 "Kittens" 0)
(count-XX-2 "Kittensxxx" 0)


(defn  string-Splosion-1
  [xs]
  (letfn [(f [xs]
            (if (== (count xs) 1)
              xs
              (str (f (into [] (drop-last xs))) xs)))]
    (f xs)))
  

  (string-Splosion-1 "CCoCodCode")
  (string-Splosion-1 "aababc")
  (string-Splosion-1 "aab")
  (string-Splosion-1 "x")
  (string-Splosion-1 "ffafadfade")
  (string-Splosion-1 "TThTheTherThere")
(string-Splosion-1 "KKiKitKittKitteKitten")
(string-Splosion-1 	"BByBye")

(defn string-Splosion-2
  [xs]
  (letfn [(f [xs a]
            (if (== (count xs) 1)
              a
              (str (f (into [] (drop-last xs)) (into [] (drop-last a))) xs)))]
    (f xs xs)))
  
(string-Splosion-2 "CCoCodCode")
(string-Splosion-2 "aababc")
(string-Splosion-2 "aab")
(string-Splosion-2 "x")
(string-Splosion-2 "ffafadfade")
(string-Splosion-2 "TThTheTherThere")
(string-Splosion-2 "KKiKitKittKitteKitten")
(string-Splosion-2 	"BByBye") 

  
  (defn array-123-1
    [xs]
    (letfn [(f [xs]
              (if (or (empty? xs) (nil? (first (rest xs))) (nil? (first (rest (rest xs)))))
                false
                (if (and (== (first xs) 1) (== (first (rest xs)) 2) (== (first (rest (rest xs))) 3))
                  true
                  (f (rest xs)))))]
      (f xs)))


(array-123-1 [1, 1, 2, 3, 1])
(array-123-1 [1, 1, 2, 4, 1])
(array-123-1 [1, 1, 2, 1, 2, 3])
(array-123-1 [1, 1, 2, 1, 2, 1])
(array-123-1 [1, 2, 3, 1, 2, 3])
(array-123-1 [1, 2, 3])
(array-123-1 [1, 1, 1])
(array-123-1 [1, 2])
(array-123-1 [1])
(array-123-1 [])

(defn array-123-2
  [xs]
  (letfn [(f [xs a]
            (if (or (empty? xs) (nil? (first (rest xs))) (nil? (first (rest (rest xs)))))
              a
              (if (and (== (first xs) 1) (== (first (rest xs)) 2) (== (first (rest (rest xs))) 3))
                true
                (f (rest xs) a))))]
    (f xs false)))

(array-123-2 [1, 1, 2, 3, 1])
(array-123-2 [1, 1, 2, 4, 1])
(array-123-2 [1, 1, 2, 1, 2, 3])
(array-123-2 [1, 1, 2, 1, 2, 1])
(array-123-2 [1, 2, 3, 1, 2, 3])
(array-123-2 [1, 2, 3])
(array-123-2 [1, 1, 1])
(array-123-2 [1, 2])
(array-123-2 [1])
(array-123-2 [])

(defn string-x-1
  [xs]
  (letfn [(f [xs n]
            (if (empty? xs)
              "  "
              (if (== 0 n)
                (str (first xs) (f (rest xs) (inc n)))
                (if (and (= \x (first xs)) (> (count xs) 1))
                  (f (rest xs) (inc n))
                  (str (first xs) (f (rest xs) (inc n)))))))]
    (f xs 0)))

(string-x-1 "xxHxix")
(string-x-1 "abxxxcd")
(string-x-1 "xabxxxcdx")
(string-x-1 "xKittenx")
(string-x-1 "Hello")
(string-x-1 "xx")
(string-x-1 "x")
(string-x-1 "")

(defn string-x-2
  [xs]
  (letfn [(f [xs n acc]
            (if (empty? xs)
              acc
              (if (== 0 n)
                (str (first xs) (f (rest xs) (inc n) (str acc)))
                (if (and (= \x (first xs)) (> (count xs) 1))
                  (f (rest xs) (inc n) (str acc))
                  (str (first xs) (f (rest xs) (inc n) acc))))))]
    (f xs 0 "")))


(string-x-2 "xxHxix")
(string-x-2 "abxxxcd")
(string-x-2 "xabxxxcdx")
(string-x-2 "xKittenx")
(string-x-2 "Hello")
(string-x-2 "xx")
(string-x-2 "x")
(string-x-2 "")

(defn alt-Pairs-1
  [a]
  (letfn [(f [xs i]
            (if (== (count xs) i)
              ""
              (if (or (== i 0) (== i 1) (== i 4) (== i 5) (== i 8) (== i 9))
                (str (subs xs i (+ i 1)) (f xs (inc i)))
                (f xs (inc i)))))]
    (f a 0)))

(alt-Pairs-1 "kitten")
(alt-Pairs-1 "Chocolate")
(alt-Pairs-1 "CodingHorror")
(alt-Pairs-1 "yak")
(alt-Pairs-1 "ya")
(alt-Pairs-1  "y")
(alt-Pairs-1 "")
(alt-Pairs-1 "ThisThatTheOther")


(defn alt-Pairs-2
  [a]
  (letfn [(f [xs i acc]
            (if (== (count xs) i)
              acc
              (if (or (== i 0) (== i 1) (== i 4) (== i 5) (== i 8) (== i 9))
                (f xs (inc i) (str acc (subs xs i (+ i 1))))
                (f xs (inc i) acc))))]
    (f a 0 "")))

(alt-Pairs-2 "kitten")
(alt-Pairs-2 "Chocolate")
(alt-Pairs-2 "CodingHorror")
(alt-Pairs-2 "yak")
(alt-Pairs-2 "ya")
(alt-Pairs-2  "y")
(alt-Pairs-2 "")
(alt-Pairs-2 "ThisThatTheOther")


(defn string-yak-1
  [xs]
  (letfn [(f [xs n]
            (if (empty? xs)
              ""
              (if (and (= (first xs) \y)
                       (= (first (rest xs)) \a)
                       (= (first (rest (rest xs))) \k)
                       (> (count xs) 1))
                (f (rest (rest (rest xs))) (inc n))
                (str (first xs) (f (rest xs) (inc n))))))]
    (f xs 0)))


(string-yak-1 "yakpak")
(string-yak-1 "pakyak")
(string-yak-1 "yak123ya")
(string-yak-1 "yak")
(string-yak-1 "yakxxxyak")
(string-yak-1 "HiyakHi")
(string-yak-1 "xxxyakyyyakzzz")

(defn string-yak-2
  [xs]
  (letfn [(f [xs n acc]
            (if (empty? xs)
              acc
              (if (and (= (first xs) \y)
                       (= (first (rest xs)) \a)
                       (= (first (rest (rest xs))) \k)
                       (> (count xs) 1))
                (f (rest (rest (rest xs))) (inc n) acc)
                (f (rest xs) (inc n) (str acc (first xs))))))]
    (f xs 0 "")))

(string-yak-2 "yakpak")
(string-yak-2 "pakyak")
(string-yak-2 "yak123ya")
(string-yak-2 "yak")
(string-yak-2 "yakxxxyak")
(string-yak-2 "HiyakHi")
(string-yak-2 "xxxyakyyyakzzz")